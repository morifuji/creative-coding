import p5 from "p5";

const rand = () => Math.random()



const sketch = (p: p5) => {

    const colorRange = () => {
        return p.color(rand()*150+100, rand()*100, rand()*100+150, (rand() * 150) + 100)
    }

    let isWhiteBg = true
    p.setup = () => {
        p.createCanvas(p.windowWidth, p.windowHeight);
        p.angleMode(p.DEGREES); // 回転の単位を弧度から角度に変更
        p.background("#000"); // 背景色を設定
    };

    p.draw = () => {
        const size = rand() * 40 + 10
        console.log(colorRange())
        p.fill(colorRange())
        p.ellipse(rand() * p.width, rand() * p.height, size, size)
    }


    p.mouseClicked = (e) => {
        p.background(isWhiteBg ? "#fff" : "#000")
        isWhiteBg = !isWhiteBg
    }
}

new p5(sketch);