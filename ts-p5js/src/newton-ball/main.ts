import p5, { Vector, BLEND, ADD } from "p5";


const sketch = (p: p5) => {

    let position = p.createVector(0, 0)
    let velocity = p.createVector(0, 0)
    let force = p.createVector(12, 8)
    let mass = 1

    let acceleration: Vector

    let gravity = p.createVector(0, 1)

    p.setup = () => {

        p.createCanvas(p.windowWidth, p.windowHeight, p.P2D);
        p.background("#000")

        acceleration = force.mult(mass)
    };


    p.draw = () => {
        p.fill(0, 31)
        p.rect(0, 0, p.width, p.height)

        p.fill(255)
        p.noStroke()

        p.text("When you click on the screen, a force acts on ball.", 10, 20)


        p.fill(100 + p.sin(p.millis() / 1000 * 2 * p.PI) * 50, 180 + p.sin(p.millis() / 1000 * 2 * p.PI + p.PI * 2 / 3) * 50, 255 + p.sin(p.millis() / 1000 * 2 * p.PI + p.PI * 4 / 3) * 50)

        // acceleration.add(gravity)

        velocity.add(acceleration)
        velocity.mult(1 - 0.02)
        position.add(velocity)
        acceleration.set(0, 0)
        p.ellipse(position.x, position.y, 20, 20)


        if (position.x <= 0 || p.width <= position.x) {
            velocity.x = -velocity.x
        }
        if (position.y <= 0 || p.height <= position.y) {
            velocity.y = -velocity.y
        }
    }



    p.touchMoved = () => {
        const vector = p.createVector(p.mouseX - position.x, p.mouseY - position.y).normalize()
        acceleration = vector.mult(mass * 2)
    }

}


new p5(sketch);