import p5 from "p5";

type Box = [[number, number], [number, number]]

let direction: "vertical" | "horizontal" = "vertical"

const sketch = (p: p5) => {
    let boxes: Box[] = []

    p.setup = () => {
        p.createCanvas(p.windowWidth, p.windowHeight);
        p.background("#000"); // 背景色を設定

        boxes = [
            [
                [p.width * 0.1, p.height * 0.1],
                [p.width * 0.9, p.height * 0.9],
            ]
        ]
        p.noFill()
        p.stroke(255)

        const dim = Math.min(p.width, p.height)

        p.push()
        p.fill(255)
        p.textSize()
        p.textAlign("center")
        p.text('Click!', p.width / 2, dim * 0.025);
        p.text('Clicking will split the largest box into two.', p.width / 2, dim * 0.05);
        p.pop()

        p.strokeWeight(dim * 0.0025)
        p.rect(boxes[0][0][0], boxes[0][0][1], boxes[0][1][0] - boxes[0][0][0], boxes[0][1][1] - boxes[0][0][1])
    };


    const chooseMostWideBox = (boxes: Box[]) => {
        const [area, mostWideBoxIndex] = boxes.reduce((current, box, i) => {
            const area = (box[1][0] - box[0][0]) * (box[1][1] - box[0][1])
            if (current[0] < area) {
                return [area, i]
            }
            return current
        }, [0, -1])

        return mostWideBoxIndex
    }

    // p.mouseClicked = () => onClick()
    p.touchEnded = () => onClick()

    const onClick = () => {

        const mostWideBoxIndex = chooseMostWideBox(boxes)
        // boxes.forEach(box => {

        const box = boxes.splice(mostWideBoxIndex, 1)[0]

        const pointXOrY = chooseLine(box)
        // divide
        if (direction === "vertical") {
            boxes.push([
                [box[0][0], box[0][1]],
                [pointXOrY, box[1][1]],
            ])
            p.push()
            p.fill(randomColor())
            p.rect(box[0][0], box[0][1], pointXOrY - box[0][0], box[1][1] - box[0][1])
            p.pop()
            boxes.push([
                [pointXOrY, box[0][1]],
                [box[1][0], box[1][1]],
            ])
            p.push()
            p.fill(randomColor())
            p.rect(pointXOrY, box[0][1], box[1][0] - pointXOrY, box[1][1] - box[0][1])
            p.pop()
        } else {
            boxes.push([
                [box[0][0], box[0][1]],
                [box[1][0], pointXOrY],
            ])
            p.push()
            p.fill(randomColor())
            p.rect(box[0][0], pointXOrY, box[1][0] - box[0][0], box[1][1] - pointXOrY)
            p.pop()
            boxes.push([
                [box[0][0], pointXOrY],
                [box[1][0], box[1][1]],
            ])
            p.push()
            p.fill(randomColor())
            p.rect(box[0][0], box[0][1], box[1][0] - box[0][0], pointXOrY - box[0][1])
            p.pop()
        }


        // draw
        if (direction === "vertical") {
            p.line(pointXOrY, box[0][1], pointXOrY, box[1][1])
        } else {
            p.line(box[0][0], pointXOrY, box[1][0], pointXOrY)
        }

        direction = direction === "vertical" ? "horizontal" : "vertical"
    }

    const randomColor = () => {
        const playhead = Math.random()
        return p.color(100 + p.sin(playhead * p.PI * 2) * 150, 100 + p.sin(playhead * p.PI * 2 + (2 * p.PI / 3)) * 150, 100 + p.sin(playhead * p.PI * 2 + (2 * p.PI * 2 / 3)) * 150)
    }
}

const chooseLine = (box: Box) => {
    let a = 0
    let b = 0
    if (direction === "vertical") {
        a = box[0][0]
        b = box[1][0]
    } else {
        a = box[0][1]
        b = box[1][1]
    }

    return a + (Math.random() * (b - a))
}

new p5(sketch);