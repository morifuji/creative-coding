/**
 * NOTE: https://glitch.com/edit/#!/p5-example?path=sketch.js%3A1%3A0
 */
import p5 from "p5";

const rand = () => Math.random()



const sketch = (p: p5) => {

    p.setup = () => {
        p.createCanvas(p.windowWidth, p.windowHeight);
    };

    p.draw = () => {
        p.background(0)     

        const dim = Math.min(p.width, p.height)

        const centerX = p.width/2
        const centerY = p.height/2

        const size = dim/2

        p.noFill()
        p.stroke(255)
        p.strokeWeight(dim * 0.015)

        const time = p.millis()/1000

        const duration = 5

        const playhead = time / duration % 1
        const rotation = p.PI * 2 * playhead

        // pushによってstateを追加する。これは簡単に削除できる
        p.push()
        
        p.translate(centerX, centerY)

        p.rotate(rotation)

        p.rectMode("center")

        p.rect(0, 0, size, size)

        p.pop()

        // popによってstateがリセットされているため、左上に配置される
        p.rectMode("center")
        p.rect(0, 0, size, size)
        
    }


}

new p5(sketch);