import p5, { Vector, BLEND, ADD } from "p5";


const sketch = (p: p5) => {

    let particles: Particle[] = []


    class Particle {
        position: Vector
        color: p5.Color
        diametor: number
        velocity: Vector
        constructor() {
            this.diametor = p.random(8, 32)
            this.position = p.createVector(p.random(p.width), p.random(p.height))
            this.color = p.color(p.random(70, 255), p.random(70, 255), p.random(70, 255))
            this.velocity = p.createVector(p.random(-4, 4), p.random(-4, 4))
        }

        draw() {
            p.fill(this.color)
            p.ellipse(this.position.x, this.position.y, this.diametor, this.diametor)
        }


        move() {
            this.position.add(this.velocity)

            if (this.position.x <= 0 || p.width <= this.position.x){
                this.velocity.x = -this.velocity.x
            }
            if (this.position.y <= 0 || p.height <= this.position.y){
                this.velocity.y = -this.velocity.y
            }
        }
    }

    p.setup = () => {

        p.createCanvas(p.windowWidth, p.windowHeight, p.P2D);
        p.background("#000")

        p.blendMode("lighter")

        p.noStroke()

        particles = [...Array(1000)].map(v => {
            return new Particle()
        })
    };


    p.draw = () => {
        p.background(0)
        particles.forEach(particle => {
            particle.draw()
            particle.move()
            // p.stroke(position.color)
            // p.point(position.x, position.y)
            // position.x = position.x + p.random(-2, 2)
            // position.y = position.y + p.random(-2, 2)
        })


        p.blendMode("source-over")
        p.fill(0, 5)
        p.noStroke()
        p.rect(0, 0, p.width, p.height)
    }
}


new p5(sketch);