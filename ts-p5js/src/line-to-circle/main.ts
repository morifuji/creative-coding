/**
 * NOTE: https://glitch.com/edit/#!/p5-example?path=sketch.js%3A1%3A0
 */
import p5 from "p5";

const rand = () => Math.random()



const sketch = (p: p5) => {

    p.setup = () => {
        p.createCanvas(p.windowWidth, p.windowHeight);
    };

    p.draw = () => {
        p.background(0)
        p.noFill()

        const dim = Math.min(p.width, p.height)

        p.stroke(255)
        p.strokeWeight(dim * 0.015)

        const centerX = p.width / 2
        const centerY = p.height / 2

        const time = p.millis() / 1000

        const duration = 3

        const playhead = time / duration % 1

        let t0 = p.sin(playhead * p.PI * 2);

        t0 = t0 * 0.5 + 0.5

        // Get a value that ping-pongs from 0 ... 1
        const pingPong = p.sin(time * 0.75 - p.PI / 2) * 0.5 + 0.5;

        // なめらか
        // const points = 2 + (98 * pingPong)
        // 急速
        const points = p.lerp(2, 100, p.pow(pingPong, 2.5));

        const angle = pingPong * p.PI * 2
        polygon(centerX, centerY, dim / 3, points, angle)

    }

    const polygon = (x: number, y: number, radius: number, sides = 3, angle = 0) => {
        p.beginShape();
        for (let i = 0; i < sides; i++) {
            const a = angle + p.TWO_PI * (i / sides);
            let sx = x + p.cos(a) * radius;
            let sy = y + p.sin(a) * radius;
            p.vertex(sx, sy);
        }
        p.endShape("close");
    }
}

new p5(sketch);