import p5, { Vector } from "p5";

const sketch = (p: p5) => {

    p.setup = () => {
        p.createCanvas(p.windowWidth, p.windowHeight);
        p.background("#000")
        p.fill(0, 127, 255)
    };


    let angle = 0
    p.draw = () => {
     
        p.background(0)

        p.push()
        p.translate(p.width/4, p.height/4)
        p.rotate(angle)
        p.rectMode("center")
        p.rect(0, 0, 300, 300)
        p.pop()

        p.push()
        p.translate(p.width/2, p.height/2)
        p.rotate(angle)

        p.rectMode("center")
        p.rect(0, 0, 200, 200)
        p.pop()

        angle += 0.1
    }
}


new p5(sketch);