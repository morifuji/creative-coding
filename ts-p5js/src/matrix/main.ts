import p5 from "p5";

const TEXT_SIZE = 15
const generateRandomString = (length: number) => {
    const candidate = '0123456789'
    return [...Array(length)].map(() => candidate[Math.floor(Math.random() * candidate.length)]).join("")
}

type Column = {
    text: string
    startYPosition: number
    speed: number
    color: p5.Color
    startedAt: number
    length: number
    amplitude: number
    size: number
}

const sketch = (p: p5) => {
    let columnCount = 0
    let columns: Column[] = []
    let font: p5.Font

    p.setup = () => {
        // FIXME: public-urlを直に仕込んでる
        font = p.loadFont("/p5js/Orbitron-VariableFont_wght.ttf")
        console.log(p.windowHeight)
        p.createCanvas(p.windowWidth, p.windowHeight);
        p.background("#000"); // 背景色を設定

        columns = [...Array(Math.floor(p.width / TEXT_SIZE) * 2)].map((_, i) => {
            return generateColumn(0)
        })

        p.textFont(font)
    }


    p.draw = () => {
        p.background(0)

        const t0 = Math.floor(p.millis() / 100)
        columns.forEach((column, columnIndex) => {
            column.text.split("").forEach((v, textIndex) => {

                const currentHead = (t0 - column.startedAt) * column.speed + column.startYPosition
                // rowCount > index > rowCount-30
                if (currentHead <= textIndex || textIndex <= currentHead - column.length) {
                    return
                }

                p.textSize(column.size)
                // rowCount-30+10 > index
                p.fill(column.color)
                if (currentHead - column.length + 5 > textIndex) {
                    const customColor = p.color(column.color)
                    // 0→1
                    const ratio = ((currentHead - column.length + 5) - textIndex) / (currentHead - column.length + 5)
                    customColor.setAlpha(200 - 200 * ratio)
                    p.fill(customColor)
                    // p.fill(0, 0, 255)
                }
                p.text(v, columnIndex * column.size + column.amplitude, (textIndex * column.size))
            })
        })

        columns = columns.map(column => {
            const currentHead = (t0 - column.startedAt) * column.speed + column.startYPosition
            if ((currentHead - column.length) * column.size > p.height) {
                console.log(currentHead - column.length)
                return generateColumn(t0)
            }

            return column
        })


    }

    p.windowResized = () => {
        p.resizeCanvas(p.windowWidth, p.windowHeight);
    }

    // p.mouseClicked = () => {
    //     const fs = p.fullscreen();
    //     p.fullscreen(!fs)
    // }

    // p.touchEnded = () => {
    //     const fs = p.fullscreen();
    //     p.fullscreen(!fs)
    // }

    const generateColumn = (startedAt: number) => ({
        text: generateRandomString(999),
        startYPosition: 0 - Math.round(Math.random() * p.height / TEXT_SIZE / 3),
        speed: 1 + Math.random(),
        color: p.color(100, 230, 100, 200),
        startedAt,
        length: 5 + Math.random() * 20,
        amplitude: -5 + (10 * Math.random()),
        size: 5 + (20 * Math.random()),
    })
}

new p5(sketch);