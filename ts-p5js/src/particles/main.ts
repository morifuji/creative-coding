import p5, { Vector } from "p5";

type Position = {
    x: number
    y: number
    color: p5.Color
}

const sketch = (p: p5) => {

    let positions: Position[] = []

    p.setup = () => {

        p.createCanvas(p.windowWidth, p.windowHeight, p.P2D);
        p.background("#000")

        positions = [...Array(10000)].map(v => {
            return {
                x: Math.round(p.random(p.width)/100)*100,
                y: Math.round(p.random(p.height)/50)*50,
                color: p.color(p.random(100, 255), p.random(100, 255) ,p.random(100, 255))
            }
        })
    };


    p.draw = () => {
        p.stroke(255)
        p.noFill()
        p.blendMode("lighter")

        positions.forEach(position=>{
            p.stroke(position.color)
            p.point(position.x, position.y)  
            position.x = position.x + p.random(-2, 2)
            position.y = position.y + p.random(-2, 2)
        })


        p.blendMode("source-over")
        p.fill(0, 5)
        p.noStroke()
        p.rect(0,0,p.width, p.height)
    }
}


new p5(sketch);