/**
 * NOTE: https://glitch.com/edit/#!/p5-example?path=sketch.js%3A1%3A0
 */
import p5 from "p5";

const rand = () => Math.random()



const sketch = (p: p5) => {

    p.setup = () => {
        p.createCanvas(p.windowWidth, p.windowHeight);
    };

    p.draw = () => {
        p.background(0)     

        const dim = Math.min(p.width, p.height)

        const centerX = p.width/2
        const centerY = p.height/2

        const size = dim/2

        const time = p.millis()/1000

        const duration = 5

        // 0..1
        const playhead = time / duration % 1

        const animation = p.sin(playhead * p.PI * 2) * 0.5 + 0.5

        const thickness = dim * 0.1 * animation

        p.noFill()

        // 色も一周させる
        // sin(θ + 0)
        // sin(θ + 120)
        // sin(θ + 240)
        p.stroke(100 + p.sin(playhead * p.PI * 2) * 100, 100 + p.sin(playhead * p.PI * 2 + (2 * p.PI / 3)) * 100, 100 + p.sin(playhead * p.PI * 2 + (2 * p.PI * 2 / 3)) * 100),

        p.strokeWeight(thickness)
        p.ellipse(centerX, centerY, size, size)
    }


}

new p5(sketch);