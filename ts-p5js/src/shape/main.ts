import p5 from "p5";

const rand = () => Math.random()



const sketch = (p: p5) => {

    p.setup = () => {
        p.createCanvas(p.windowWidth, p.windowHeight);
        p.angleMode(p.DEGREES); // 回転の単位を弧度から角度に変更
        p.background(0); // 背景色を設定

        p.noFill()

        // NOTE: https://p5js.org/reference/#/p5/strokeJoin
        p.strokeJoin("round")

        p.stroke(255)

        const dim = Math.min(p.width, p.height)
        const size = dim * 0.5

        // 15%を太さに
        p.strokeWeight(dim*0.015)

        const centerX = p.width/2
        const centerY = p.height/2

        p.rectMode("center")
        p.rect(centerX, centerY, size, size)
        p.ellipse(centerX, centerY, size, size)

        p.triangle(
            centerX,
            centerY - size / 2,
            centerX + size / 2,
            centerY + size / 2,
            centerX - size / 2,
            centerY + size / 2
        )
    };

    p.draw = () => {
    }


}

new p5(sketch);