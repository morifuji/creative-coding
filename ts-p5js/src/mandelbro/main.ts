import p5, { Vector } from "p5";

type Point = {
    x: number,
    y: number
    vector: {
        x: number,
        y: number,
    }
}

const sketch = (p: p5) => {
    let points: Point[] = []

    let lastT = 0

    p.setup = () => {
        p.createCanvas(p.windowWidth, p.windowHeight);
        p.background("#000"); // 背景色を設定


        p.noFill()
        p.stroke(255)

        points = [
            {
                x: 0,
                y: 0,
                vector: {
                    x: 1,
                    y: 1,
                }
            }
        ]
    };


    p.draw = () => {
        const t = Math.round(p.millis()/1000)
        
        if (t !== lastT) {
            divide()
            lastT = t
        }

        points = points.map(point => {
            // move
            let lastX = point.x
            let lastY = point.y

            point.x += (p.deltaTime/10) *point.vector.x
            point.y += (p.deltaTime/10) *point.vector.y

            // draw
            p.line(lastX, lastY, point.x, point.y)

            return point
        })
    }


    p.touchEnded = () => divide()

    const divide = () => {
        points = points.flatMap(point => {
            return [{
                ...point,
                vector: {
                    x: point.vector.x * p.cos(p.PI / 4) - point.vector.y * p.sin(p.PI / 4),
                    y: point.vector.x * p.sin(p.PI / 4) + point.vector.y * p.cos(p.PI / 4)
                }
            },
            {
                ...point,
                vector: {
                    x: point.vector.x * p.cos(-p.PI / 4) - point.vector.y * p.sin(-p.PI / 4),
                    y: point.vector.x * p.sin(-p.PI / 4) + point.vector.y * p.cos(-p.PI / 4)
                }
            }
            ]
        })
    }

}


new p5(sketch);