/**
 * NOTE: https://glitch.com/edit/#!/p5-example?path=sketch.js%3A1%3A0
 */
import p5 from "p5";

const rand = () => Math.random()



const sketch = (p: p5) => {

    p.setup = () => {
        p.createCanvas(p.windowWidth, p.windowHeight);
    };

    p.draw = () => {
        p.background(0)
        p.noFill()
        
        const dim = Math.min(p.width, p.height)

        p.stroke(255)
        p.strokeWeight(dim * 0.015)

        const centerX = p.width/2
        const centerY = p.height/2
    
        const time = p.millis()/1000
    
        const duration = 3

        const playhead = time / duration % 1

        let t0 = p.sin(playhead * p.PI * 2);

        t0 = t0 * 0.5 + 0.5

        const size = dim/3

        {
            const start = [p.width * 0.25, p.height / 2]
            const end = [p.width * 0.75, p.height / 2]
            const x = p.lerp(start[0], end[0], t0)
            const y = p.lerp(start[1], end[1], t0)
            p.ellipse(x, y, size, size)
        }
        
        {
            const start = [p.width /2, p.height * 0.25]
            const end = [p.width /2, p.height * 0.75]
            const x = p.lerp(start[0], end[0], t0)
            const y = p.lerp(start[1], end[1], t0)
            p.ellipse(x, y, size, size)
        }

        {
            const rotate = p.lerp(0, p.PI * 2, t0)
            p.ellipse(centerX + (dim / 3 * p.cos(rotate)), centerY + (dim / 3 * p.sin(rotate)), size, size)
            p.ellipse(centerX + (dim / 3 * -p.cos(rotate)), centerY + (dim / 3 * -p.sin(rotate)), size, size)
        }

    }


}

new p5(sketch);