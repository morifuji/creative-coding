import p5, { Vector } from "p5";

const sketch = (p: p5) => {
    p.setup = () => {
        p.createCanvas(p.windowWidth, p.windowHeight);
        // p.colorMode("hsb", 360, 100, 100, 100)
        p.colorMode("rgb")
        p.background("#000")
        p.rectMode("center")
        
    };

    let passedMillTime: number = 0
    let isPaused = false


    p.touchEnded = () => {
        isPaused = !isPaused
    }

    p.draw = () => {
        if (!isPaused){
            passedMillTime += p.deltaTime
        }

        // gradient.addColorStop(1, p.color(100, 170, 100, 100))

        // p.drawingContext.fillStyle = linearGradient(p.width/2-200, p.height/2-200,p.width/2+200, p.height/2+200, p.color(310, 100, 100, 100), p.color(250, 120, 100, 100))
        // p.rect(p.width/2, p.height/2, 400, 400, 50)

        p.background("#000")

        // const mills = pausedTime===null ? p.millis() : pausedTime
        
        const t = passedMillTime/1000
        const t1 = passedMillTime/2000
        const t2 = passedMillTime/p.PI*100

        // p.noStroke()

        // p.noFill()
        // p.strokeWeight(50+30*p.sin(t1*p.PI*2))
        // p.drawingContext.strokeStyle = radialGradient(p.width / 2-60, p.height / 2-120, 0, p.width / 2-60, p.height / 2-120, 600, colorHueCircle(t, 170, 50), colorHueCircle(t, 170, 50))
        // p.ellipse(p.width / 2, p.height / 2, 400, 400)

        // p.fill(colorHueCircle(0 ,150, 50))
        // p.ellipse(p.width / 2-100, p.height / 2-100, 100, 100)
        // p.fill(colorHueCircle(0.333 ,150, 50))
        // p.ellipse(p.width / 2+100, p.height / 2-100, 100, 100)
        // p.fill(colorHueCircle(0.666 ,150, 50))
        // p.ellipse(p.width / 2+100, p.height / 2+100, 100, 100)
        circle(t, t1, p.width/2, p.height/2, 400)

        const xRatio = p.mouseX/p.width
        const yRatio = p.mouseY/p.height
        
        const r = 60
        for (let heightIndex = 0; heightIndex < p.height/(r+40); heightIndex++) {
            for (let widthIndex = 0; widthIndex < p.width/(r+40); widthIndex++) {
                circle(t, t1, (r+40)*widthIndex+p.sin(t2*p.PI*2)* xRatio, (r+40)*heightIndex+p.sin(t2*p.PI*2)* xRatio, r*yRatio)
            }
        }


        p.fill(255)
        p.text("Clicking will stop rotate colors in circle.", p.width/5,p.height/5)
    }

    p.windowResized = () => {
        p.resizeCanvas(p.windowWidth, p.windowHeight);
    }


    const circle = (t: number, t1: number, x: number, y: number, r: number) => {
        p.noFill()
        p.colorMode("hsb", 360, 100, 100, 1)
        p.strokeWeight(r*1/8+p.sin(t1*p.PI*2)*r/12)
        p.drawingContext.strokeStyle = conicGradient(0, x, y,[
            p.color((0+t*360)%360, 100, 100),
            p.color((60+t*360)%360, 100, 100),
            p.color((120+t*360)%360, 100, 100),
            p.color((180+t*360)%360, 100, 100),
            p.color((240+t*360)%360, 100, 100),
            p.color((300+t*360)%360, 100, 100),
        ])
        p.ellipse(x, y, r, r)
    }

    const colorHueCircle = (t: number, base: number, amplitude: number) => {
        return p.color(
            base + p.sin(p.PI*(0/3+2*t))*amplitude,
            base + p.sin(p.PI*(2/3+2*t))*amplitude,
            base + p.sin(p.PI*(4/3+2*t))*amplitude
        )
    }


    const linearGradient = (sX: number, sY: number, eX: number, eY: number, sColor: any, eColor: any) => {
        let gradient = p.drawingContext.createLinearGradient(
            sX, sY, eX, eY
        )
        gradient.addColorStop(0, sColor)
        gradient.addColorStop(1, eColor)
        return gradient
    }


    const radialGradient = (sX: number, sY: number, sR: number, eX: number, eY: number, eR: number, sColor: any, eColor: any) => {
        let gradient = p.drawingContext.createRadialGradient(
            sX, sY, sR, eX, eY, eR
        )
        gradient.addColorStop(0, sColor)
        gradient.addColorStop(0.5, eColor)
        gradient.addColorStop(1, p.color(0))
        return gradient
    }

    const conicGradient = (sA: number, cX: number, cY: number,colors: p5.Color[]) => {
        let gradient = p.drawingContext.createConicGradient(
            sA, cX, cY
        )
        colors.forEach((color, i) => {
            gradient.addColorStop(i/colors.length, color)
        });       
        gradient.addColorStop(1, colors[0])
        return gradient
    }
}


new p5(sketch);