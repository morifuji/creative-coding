import p5, { Vector } from "p5";

const LENGTH = 30

const sketch = (p: p5) => {
    let x:number
    let y:number

    let positions: [number, number][] = []

    p.setup = () => {
        p.createCanvas(p.windowWidth, p.windowHeight);
        p.background("#000")

        p.stroke(255)
        p.noFill()

        x = p.width/2
        y = p.height/2
    };

    let targetX = 0
    let targetY = 0

    p.draw = () => {
        let targetX = p.mouseX 
        let targetY = p.mouseY
        
        positions.push([x, y])
        
        positions = positions.splice(positions.length-LENGTH)

        p.background("#000")
        positions.forEach(([x, y], i) => {
            p.fill(255, 255*i/LENGTH)
            p.ellipse(x, y, 30, 30)
        })
        const diffX = p.mouseX - x
        const diffY = p.mouseY - y

        const d = Math.sqrt(diffX**2+diffY**2)
        const dx = diffX/d
        const dy = diffY/d

        x += dx * p.deltaTime
        y += dy * p.deltaTime

    }
}


new p5(sketch);