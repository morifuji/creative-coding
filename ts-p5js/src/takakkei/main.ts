import p5, { Vector } from "p5";

const sketch = (p: p5) => {

    p.setup = () => {
        p.createCanvas(p.windowWidth, p.windowHeight);
        p.background("#000")
        p.noFill()
    };


    p.draw = () => {
        p.background("#000")
        const t = p.millis()/5000
        const t0 = 10*p.sin(t*p.PI*2)

        takakkei(t0+12, p.width/2, p.height/2,100)
    }

    const takakkei = (n: number, x: number, y: number, r: number) => {
        // p.fill(255)
        p.stroke(255)
        p.beginShape();
        for (let i = 0; i < n; i++) {
            if (i==n-1) {
                p.vertex(x + r*p.sin(2*p.PI*i/n), y + r*p.cos(2*p.PI*i/n))
                continue
            }
            p.vertex(x + r*p.sin(2*p.PI*i/n)+r*p.noise(p.mouseX, p.mouseY, n)-r/2, y + r*p.cos(2*p.PI*i/n)+r*p.noise(p.mouseY, p.mouseX, n)-r/2)
            
        }
        p.endShape("close")
    }
}


new p5(sketch);