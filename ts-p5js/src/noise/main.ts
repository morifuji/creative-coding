/**
 * NOTE: https://glitch.com/edit/#!/p5-example-noise-lines?path=sketch.js%3A15%3A0
 */
import p5 from "p5";
import { openSimplexNoise } from "../../lib/simplexNoise"

let minFrequency = 0.5;
let maxFrequency = 2;
let minAmplitude = 0.05;
let maxAmplitude = 0.3;

const simplex = openSimplexNoise(500)


const sketch = (p: p5) => {

    p.setup = () => {
        p.createCanvas(p.windowWidth, p.windowHeight);
    };

    p.draw = () => {
        p.background(0)
        p.noFill()

        const dim = Math.min(p.width, p.height)


        const frequency = p.lerp(minFrequency, maxFrequency, p.mouseX / p.width);
        const amplitude = p.lerp(minAmplitude, maxAmplitude, p.mouseY / p.height);

        // Draw the background
        p.noFill();
        p.stroke(255);
        p.strokeWeight(dim * 0.0075);

        const time = p.millis() / 1000;
        const rows = 10;

        // Draw each line
        for (let y = 0; y < rows; y++) {
            // Determine the Y position of the line
            const v = rows <= 1 ? 0.5 : y / (rows - 1);
            const py = v * p.height;
            drawNoiseLine({
                v,
                start: [0, py],
                end: [p.width, py],
                amplitude: amplitude * p.height,
                frequency,
                time: time * 0.5,
                steps: 150
            });
        }
    }

    function drawNoiseLine(opt) {
        const {
            v,
            start,
            end,
            steps = 10,
            frequency = 1,
            time = 0,
            amplitude = 1
        } = opt;

        const [xStart, yStart] = start;
        const [xEnd, yEnd] = end;

        // Create a line by walking N steps and interpolating
        // from start to end point at each interval
        p.beginShape();
        for (let i = 0; i < steps; i++) {
            // Get interpolation factor between 0..1
            const t = steps <= 1 ? 0.5 : i / (steps - 1);

            // Interpolate X position
            const x = p.lerp(xStart, xEnd, t);

            // Interpolate Y position
            let y = p.lerp(yStart, yEnd, t);

            // // Offset Y position by noise
            y += (simplex.noise3D(60*t * frequency + time, v * frequency, 15+time)) * amplitude;

            // Place vertex
            p.vertex(x, y);
        }
        p.endShape();
    }
}


new p5(sketch)