set -ex
yarn build --public-url /p5js
rm -rf ~/blog/static/p5js
mv ./dist p5js
mv p5js ~/blog/static/p5js
