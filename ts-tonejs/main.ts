import * as Tone from "tone"


let i = 0
const onClick = () => {


  //create a synth and connect it to the main output (your speakers)
  const synth = new Tone.Synth().toDestination();

  const now = Tone.now()

  // // trigger the attack immediately
  // synth.triggerAttack("C4", now)
  // // wait one second before triggering the release
  // synth.triggerRelease(now + 1)
  synth.triggerAttackRelease(`C${i}`, "8n");
  i++
  i = i%8
}

document.querySelector("button")?.addEventListener("click", onClick)
